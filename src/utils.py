import json
from typing import Union

import aiohttp
from aiocache import Cache

from settings import API_URL, CACHE_TTL, REDIS

if REDIS:
    cache = Cache(Cache.REDIS, endpoint="127.0.0.1", port=6379, namespace="main")
else:
    cache = Cache(Cache.MEMORY)


async def get_client(client_id: int) -> Union[dict, None]:
    cache_key = f'client_{client_id}'
    return await fetch_from_api(cache_key, f'{API_URL}/clients/{client_id}')


async def get_tariff(tariff_id: int) -> Union[dict, None]:
    cache_key = f'tariff_{tariff_id}'
    return await fetch_from_api(cache_key, f'{API_URL}/tariffs/{tariff_id}')


async def fetch_from_api(cache_key, url):
    client_dict = await cache.get(cache_key, None)
    if client_dict is dict:
        return client_dict
    elif client_dict == '':
        return None
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status == 404:
                await cache.set(cache_key, '', ttl=CACHE_TTL)
                return None
            text = await resp.text()
            client_dict = json.loads(text)
            await cache.set(cache_key, client_dict, ttl=CACHE_TTL)
            return client_dict
