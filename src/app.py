import asyncio
import logging

import jwt
import uvloop
from aiohttp import web
from aiohttp_jwt import JWTMiddleware
from aiohttp_swagger import *

import settings
from api import routes
from db import init
from settings import JWT_SECRET

log = logging.getLogger(__name__)


async def main():
    await init()

    app = web.Application(middlewares=[
        JWTMiddleware(JWT_SECRET, whitelist=[
            r'/api*'
        ]),
    ])
    app.add_routes(routes)
    setup_swagger(app, security_definitions={
        'bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    })
    return app


if __name__ == '__main__':
    if settings.UVLOOP:
        log.warning("Enabling uvloop")
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    loop = asyncio.get_event_loop()
    if settings.DEBUG:
        log.warning("Enabling debug")
        loop.set_debug(True)

    for client_id in range(1,10):
        token = jwt.encode({"client_id": client_id}, JWT_SECRET)
        log.warning(f"JWT example {client_id}: Bearer {token.decode('utf-8')}")
    app = loop.run_until_complete(main())

    if settings.DEBUG:
        web.run_app(app, host='0.0.0.0', access_log_format='%t "%r" %s %b %Tf %P')
    else:
        web.run_app(app, host='0.0.0.0', access_log=None)
