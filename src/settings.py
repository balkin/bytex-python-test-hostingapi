import os

from marshmallow import fields

DB=os.getenv('DB', 'postgres://user:password@localhost/accounts')
CACHE_TTL = int(os.getenv('CACHE_TTL', 60))
API_URL = os.getenv('API_URL', 'http://api.test.brn.pw')
DEBUG=os.getenv('DEBUG', 'false') in fields.Boolean.truthy
UVLOOP=os.getenv('UVLOOP', 'false') in fields.Boolean.truthy
REDIS=os.getenv('REDIS', 'false') in fields.Boolean.truthy
JWT_SECRET=os.getenv('JWT_SECRET', 'SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c')
