from marshmallow import Schema, fields


class ClientSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    username = fields.String()
    email = fields.String()


class TariffSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    size = fields.Integer()
    websites = fields.Integer()
    databases = fields.Integer()


class HostingSchema(Schema):
    id = fields.Integer()
    success = fields.Boolean()
    status = fields.String(default='TRIAL')
    client = fields.Nested(ClientSchema)
    tariff = fields.Nested(TariffSchema)


class RegisterRequestSchema(Schema):
    client_id = fields.Integer(required=False, validate=lambda x: x > 0)
    tariff_id = fields.Integer(required=True, validate=lambda x: x > 0)
