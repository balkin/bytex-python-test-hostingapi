from json import JSONDecodeError

from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response
from marshmallow import ValidationError

from db import Account
from schemas import HostingSchema, RegisterRequestSchema
from utils import get_client, get_tariff

routes = web.RouteTableDef()


@routes.get('/v1/ping')
async def ping(request: Request):
    """
    ---
    description: This end-point allow to test that service is up.
    tags:
    - Health check
    produces:
    - text/plain
    responses:
        "200":
            description: successful operation. Return "pong" text
        "405":
            description: invalid HTTP Method
    """
    return web.Response(text="pong")


@routes.post('/v1/register')
async def post_handler(request: Request):
    """
    description: Fake register
    tags:
    - Fake API
    produces:
    - application/json
    parameters:
    - in: body
      name: body
      description: Created user object
      required: false
      schema:
        type: object
        properties:
          client_id:
            type: integer
            format: int64
          tariff_id:
            type: integer
            format: int64
    responses:
        "200":
            description: Return object
        "400":
            description: Invalid request
    security:
        bearer: []
    """
    raw_body = await request.text()
    try:
        validated_data = RegisterRequestSchema().loads(raw_body)
        tariff_id = validated_data.get('tariff_id')
        client_id = validated_data.get('client_id') or request['payload']['client_id']

        tariff = await get_tariff(tariff_id)
        if tariff is None:
            return Response(status=404, text=f"Tariff {tariff_id} not found")
        client = await get_client(client_id)
        if client is None:
            return Response(status=404, text=f"Client {client_id} not found")

        account = await Account.create(client_id=client_id, tariff_id=tariff_id)

        serializer = HostingSchema()
        json = serializer.dump({
            "id": account.id,
            "success": True,
            "status": "TRIAL",
            "client": client,
            "tariff": tariff,
        })
        return web.json_response(json)
    except ValidationError as e:
        strings = [f'{k}: {"".join(v)}' for k, v in e.messages.items()]
        return web.json_response(data={"success": False, "errors": strings})
    except JSONDecodeError as e:
        return web.json_response(data={"success": False, "errors": [f'JSON decode error: {e.__str__()}']})
