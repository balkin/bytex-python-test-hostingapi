from tortoise import Model, fields, Tortoise

from settings import DB


class Account(Model):
    id = fields.IntField(pk=True)
    client_id = fields.IntField()
    tariff_id = fields.IntField()

    def __str__(self):
        return f"Account {id}"

async def init():
    # Here we connect to a SQLite DB file.
    # also specify the app name of "models"
    # which contain models from "app.models"
    await Tortoise.init(
        db_url=DB,
        modules={'models': ['db']}
    )
    # Generate the schema
    await Tortoise.generate_schemas()
