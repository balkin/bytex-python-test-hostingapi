FROM python:3.7-alpine

EXPOSE 8080
WORKDIR /app/src
COPY requirements.txt /app

RUN apk update \
    && apk add bash curl busybox-extras vim sed python3-dev make gcc musl-dev \
    && pip install -r /app/requirements.txt

COPY . /app
ENTRYPOINT ["python3", "app.py"]
